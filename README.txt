Wi-Fi mute on call
==================

Automatically mute the Wi-Fi connection when calling or receiving a call.
Restore it when hanging up.

This is a pet project only known to work on an actual Android 5.0.2 phone.
I didn't waste more time for all the API and security changes of later API
versions.

Feel free to fork if you need it to work on your phone.

It's also my first Android project right after the tutorial, so don't mind the
quality.
