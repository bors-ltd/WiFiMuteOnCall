/*
 * Originally taken from https://gist.github.com/ftvs/e61ccb039f511eb288ee
 */
package fr.borsltd.wifimuteoncall

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.wifi.WifiManager
import android.preference.PreferenceManager
import android.telephony.TelephonyManager
import android.util.Log
import android.util.Log.println


class PhoneStateReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        println(Log.DEBUG, "PhoneStateReceiver", "onReceive intent=${intent.action}")

        val stateStr = intent.extras!!.getString(TelephonyManager.EXTRA_STATE)
        println(Log.DEBUG, "PhoneStateReceiver", "onReceive stateStr=$stateStr")
        var state = TelephonyManager.CALL_STATE_IDLE
        when (stateStr) {
            TelephonyManager.EXTRA_STATE_OFFHOOK -> state = TelephonyManager.CALL_STATE_OFFHOOK
            TelephonyManager.EXTRA_STATE_RINGING -> state = TelephonyManager.CALL_STATE_RINGING
        }
        onCallStateChanged(context, state)
    }

    // Deals with actual events

    // Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    // Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    private fun onCallStateChanged(context: Context, state: Int) {
        val internalPreferences: SharedPreferences = context.getSharedPreferences("internal", Context.MODE_PRIVATE)
        val lastState = internalPreferences.getInt(context.getString(R.string.pref_last_state), TelephonyManager.CALL_STATE_IDLE)

        // 0 = IDLE
        // 1 = RINGING
        // 2 = OFFHOOK
        println(Log.DEBUG, "PhoneStateReceiver", "onCallStateChanged lastState=$lastState -> state=$state")

        if (lastState == state){
            //No change, debounce extras
            return
        }

        when (state) {
            TelephonyManager.CALL_STATE_OFFHOOK -> {
                onCallStarted(context)
            }
            TelephonyManager.CALL_STATE_IDLE -> {
                /*
                Sadly it's less reliable than PhoneListener but at least it's still active
                when the app is closed. So let's always pretend the call has ended and ignore
                if the Wi-Fi was actually not muted. And find out how to keep a listener active.
                 */
                onCallEnded(context)
            }
        }

        with (internalPreferences.edit()) {
            putInt(context.getString(R.string.pref_last_state), state)
            apply()
        }
    }

    private fun onCallStarted(context: Context) {
        println(Log.DEBUG, "PhoneStateReceiver", "onCallStarted")

        val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val muterEnabled = sharedPreferences.getBoolean(context.getString(R.string.pref_muter_enabled), true)

        if (!muterEnabled) {
            println(Log.DEBUG, "PhoneStateListener", "I must NOT mute Wi-Fi")
            return
        }

        println(Log.DEBUG, "PhoneStateListener", "I MUST mute Wi-Fi")

        val applicationContext = context.applicationContext
        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        println(Log.DEBUG, "PhoneStateListener", "Wi-Fi is currently enabled? ${wifiManager.isWifiEnabled}")
        val internalPreferences: SharedPreferences = context.getSharedPreferences("internal", Context.MODE_PRIVATE)

        if (!wifiManager.isWifiEnabled) {
            println(Log.DEBUG, "PhoneStateListener", "I don't fiddle with Wi-Fi")
            with (internalPreferences.edit()) {
                putBoolean(context.getString(R.string.pref_wifi_disabled), false)
                apply()
            }
            return
        }

        println(Log.DEBUG, "PhoneStateListener", "I disable Wi-Fi")
        wifiManager.setWifiEnabled(false) // Ignore errors
        with (internalPreferences.edit()) {
            putBoolean(context.getString(R.string.pref_wifi_disabled), true)
            apply()
        }
    }

    private fun onCallEnded(context: Context) {
        println(Log.DEBUG, "PhoneStateReceiver", "onCallEnded")

        val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val muterEnabled = sharedPreferences.getBoolean(context.getString(R.string.pref_muter_enabled), true)
        val internalPreferences: SharedPreferences = context.getSharedPreferences("internal", Context.MODE_PRIVATE)
        val wifiDisabled = internalPreferences.getBoolean(context.getString(R.string.pref_wifi_disabled), false)

        if (!muterEnabled) {
            println(Log.DEBUG, "PhoneStateReceiver", "I must NOT restore Wi-Fi")
            return
        }

        if (!wifiDisabled) {
            println(Log.DEBUG, "PhoneStateReceiver", "Nothing to do")
            return
        }

        println(Log.DEBUG, "PhoneStateReceiver", "I MUST restore Wi-Fi")

        val applicationContext = context.applicationContext
        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        println(Log.DEBUG, "PhoneStateReceiver", "Wi-Fi is currently enabled? ${wifiManager.isWifiEnabled}")

        wifiManager.setWifiEnabled(true) // Ignore errors
    }
}
